# Technologie sieciowe - Zadania 0-9

## Źródła
[Link do zadań](https://gsliwinski.wi.zut.edu.pl/?page_id=13)
[Samba tutorial](https://adrianmejia.com/how-to-set-up-samba-in-ubuntu-linux-and-access-it-in-mac-os-and-windows/)


## Przygotowanie środowiska
Do stworzenia środowiska na którym wykonywał będe zadania użyłem obrazu docker o nazwie: **rastasheep/ubuntu-sshd:18.04**

Polecenie którym utworzyłem kompletny kontener:
```bash
sudo docker run -d -P --name ubuntu-18_04-port-prefix-491 rastasheep/ubuntu-sshd:18.04
```

IP kontenera sprawdziłem za pomocą polecenia:
```bash
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ubuntu-18_04-port-prefix-491 
```

Do kontenera zalogowałem się poprzez ssh na użytkownika **root**:
```bash
ssh root@$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ubuntu-18_04-port-prefix-491)
```
Hasłem jest **root**.

Dokonałem aktualizacji pakietów systemowych za pomocą poleceń:
```bash
apt update && apt upgrade
```

Zainstalowałem pakiet **sudo** oraz **nano**:
```bash
apt install sudo nano
``` 

Stworzyłem użytkownika o nazwie **igormaculewicz** i haśle **12345678** poleceniem:
```bash
adduser igormaculewicz
```

a także dodałem go do sudoersów poleceniem:
```bash
usermod -aG sudo igormaculewicz
```

Wylogowałem się poleceniem:
```bash
logout
```

Zalogowałem się na użytkownika **igormaculewicz** poleceniem:
```bash
ssh igormaculewicz@$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ubuntu-18_04-port-prefix-491)
```

## Zadania

### Zadanie 1
Uzyskałem uprawnienia administratora za pomocą polecenia:
```bash
sudo -s
```

Dokonałem instalacji następujących pakietów **bzip2**, **unzip**, **zip**, **bind9** poleceniem:
```bash
apt install bzip2 unzip zip bind9
```

Przeszedłem do katalogu **/etc/bind** poleceniem:
```bash
cd /etc/bind
```

Następnie otworzyłem plik **named.conf.default-zones** za pomoca polecenia:
```bash
nano -w named.conf.default-zones 
```
i skopiowałem nastepujące linie:
```text
zone "localhost" {
        type master;
        file "/etc/bind/db.local";
}; 
```
do pliku **named.conf.local**.

Następnie utworzyłem plik **db.lab.lokalne** z istniejącego pliku **db.local** poleceniem:
```bash
cp db.local db.lab.lokalne
```
i podmieniłem wszystkie wystepienia **localhost** na **lab.lokalne** poleceniem:
```bash
sed -i 's/localhost/lab.lokalne/g' db.lab.lokalne 
```

Zrestartowałem usługę **bind9** poleceniem:
```bash
/etc/init.d/bind9 restart
```

Uzyskałem lokalny adres ip maszyny za pomoca polecenia:
```bash
ifconfig | grep 172.*
```

Otworzyłem do edycji plik **db.lab.lokalne** i dodałem na końcu pliku dwa rekordy **www** oraz **www1**:
```text
www      IN    A      172.17.0.2
www1     IN    CNAME  www
```

Zainstalowałem pakiet **dnsutils** poleceniem:
```bash
apt install dnsutils
```

Wykonałem polecenie **nslookup** dla serwera **127.0.0.1** i domeny **www.lab.lokalne**. Wynik:
```text
root@01c4a2da1d36:/etc/bind# nslookup
> server 127.0.0.1
Default server: 127.0.0.1
Address: 127.0.0.1#53
> www.lab.lokalne
Server:         127.0.0.1
Address:        127.0.0.1#53

Name:   www.lab.lokalne
Address: 172.17.0.2
```

Wyszedłem poleceniem:
```bash
exit
```

### Zadanie 2
Uzyskałem uprawnienia administratora za pomocą polecenia:
```bash
sudo -i
```

Zainstalowałem pakiety **apache2** **php7.2** **libapache2-mod-php7.2** **mysql-server** **php7.2-mysql** **phpmyadmin** poleceniem:
```bash
apt install apache2 php7.2 libapache2-mod-php7.2 mysql-server php7.2-mysql phpmyadmin
```

#### Mysql

> :warning: **Dla mysql-server5.7+**: Ze względów bezpieczeństwa nie ma możliwości zalogowania się na konto root.  
> Należy utworzyć dedykowane konto.  

Aby utworzyć dedykowane konto, zalogowałem się na użytkownika root do bazy danych:
```bash
mysql --user=root
```

Wykonałem następujące zapytania:
```mysql
CREATE USER 'phpmyadmin'@'localhost' IDENTIFIED BY '12345678';
GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```
+ Pierwsze zapytanie tworzy użytkownika **phpmyadmin** identyfikującego się hasłem **12345678**.
+ Drugie zapytanie nadaje mu uprawnienia do wszystkich tabel.
+ Trzecie przeładowuje uprawnienia.

Wyszedłem z klienta **mysql** poleceniem:
```mysql
exit
```

Sprawdziłem poprawność działania użytkownika logując się na niego poleceniem:
```bash
mysql --user='phpmyadmin' --password=12345678
```
i wykonując następujące zapytanie:
```mysql
SELECT * from mysql.user;
```

Wyszedłem z klienta **mysql** poleceniem:
```mysql
exit
```

Następnie na maszynie hosta wpisałem adres:
```url
http://172.17.0.2/phpmyadmin
```
i zalogowałem się za pomocą użytkownika **phpmyadmin** oraz hasła **12345678**.

Aby utworzyć konto **joomla** zalogowałem się klientem mysql na użytkownika **phpmyadmin** hasłem **12345678** poleceniem:
```bash
mysql --user='phpmyadmin' --password=12345678
```

Zapytaniem utworzyłem baze danych o nazwie **joomla**:
```mysql
CREATE DATABASE joomla;
```

Następnie, poniższymi zapytaniami utworzyłem użytkownika **joomla** identyfikującego się hasłem **12345678**  
i nadałem mu uprawnienia do wszystkich tabel w bazie danych o nazwie **joomla**:
```mysql
CREATE USER 'joomla'@'localhost' IDENTIFIED BY '12345678';
GRANT ALL PRIVILEGES ON joomla.* TO 'joomla'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```

Wyszedłem z klienta **mysql** poleceniem:
```mysql
exit
```

Zalogowałem się na użytkownika **joomla** który identyfikuje się hasłem **12345678** następującym poleceniem:
```bash
mysql --user='joomla' --password=12345678
```

Zapytaniem **'show databases'** sprawdziłem czy mam dostęp do bazy danych **joomla**. Wynik:
```text
+--------------------+
| Database           |
+--------------------+
| information_schema |
| joomla             |
+--------------------+
2 rows in set (0.00 sec)
```

Wyszedłem z klienta **mysql** poleceniem:
```mysql
exit
```

#### Apache

Przeszedłem do katalogu **/var/www/html** poleceniem:
```bash
cd /var/www/html
```

Usunąłem plik **index.html** poleceniem:
```bash
rm -f index.html
```

Utworzyłem katalog o nazwie **joomla**, ustawiłem właściciela katalogu oraz wszystkich podrzednych plików na **www-data** i przeszedłem do niego poleceniem:
```bash
mkdir joomla && chown -R www-data:www-data "$_" && cd "$_"
```

Ściągnąłem najnowszy pakiet **joomla** w wersji **3.9.18**, wypakowałem go a nastepnie usunąłem archiwum poleceniem:
```bash
wget https://downloads.joomla.org/pl/cms/joomla3/3-9-18/Joomla_3-9-18-Stable-Full_Package.zip?format=zip -O joomla.zip && unzip "$_" && rm "$_"
```

Na maszynie hosta wszedłem wszedłem na adres **http://172.17.0.2/joomla** i dokonałem instalacji joomla. Wynikowa konfiguracja:
```text
 <?php
class JConfig {
        public $offline = '0';
        public $offline_message = 'Witryna została wyłączona ze względu na prace konserwacyjne.<br />Proszę zajrzyj tu ponownie za jakiś czas.';
        public $display_offline_message = '1';
        public $offline_image = '';
        public $sitename = 'Laboratoria Sieci';
        public $editor = 'tinymce';
        public $captcha = '0';
        public $list_limit = '20';
        public $access = '1';
        public $debug = '0';
        public $debug_lang = '0';
        public $debug_lang_const = '1';
        public $dbtype = 'mysqli';
        public $host = 'localhost';
        public $user = 'joomla';
        public $password = '12345678';
        public $db = 'joomla';
        public $dbprefix = 'vgpj1_';
        public $live_site = '';
        public $secret = '9CpRJtz9H62raX0L';
        public $gzip = '0';
        public $error_reporting = 'default';
        public $helpurl = 'https://help.joomla.org/proxy?keyref=Help{major}{minor}:{keyref}&lang={langcode}';
        public $ftp_host = '127.0.0.1';
        public $ftp_port = '21';
        public $ftp_user = '';
        public $ftp_pass = '';
        public $ftp_root = '';
        public $ftp_enable = '0';
        public $offset = 'UTC';
        public $mailonline = '1';
        public $mailer = 'mail';
        public $mailfrom = 'igor.maculewicz@gmail.com';
        public $fromname = 'Laboratoria Sieci';
        public $sendmail = '/usr/sbin/sendmail';
        public $smtpauth = '0';
        public $smtpuser = '';
        public $smtppass = '';
        public $smtphost = 'localhost';
        public $smtpsecure = 'none';
        public $smtpport = '25';
        public $caching = '0';
        public $cache_handler = 'file';
        public $cachetime = '15';
        public $cache_platformprefix = '0';
        public $MetaDesc = 'Testowa joomla na potrzeby laboratoriów z sieci komputerowych';
        public $MetaKeys = '';
        public $MetaTitle = '1';
        public $MetaAuthor = '1';
        public $MetaVersion = '0';
        public $robots = '';
        public $sef = '1';
        public $sef_rewrite = '0';
        public $sef_suffix = '0';
        public $unicodeslugs = '0';
        public $feed_limit = '10';
        public $feed_email = 'none';
        public $log_path = '/var/www/html/joomla/administrator/logs';
        public $tmp_path = '/var/www/html/joomla/tmp';
        public $lifetime = '15';
        public $session_handler = 'database';
        public $shared_session = '0';
```

Na koniec usunąłem katalog **installation** poleceniem:
```bash
rm -rf installation
```

### Zadanie 3

#### Webmin
Przeszedłem do katalogu aktualnego użytkownika za pomoca polecenia:
```bash
cd ~
```

Utworzyłem katalog **Downloads** i przeszedłem do niego poleceniem:
```bash
mkdir Downloads && cd "$_"
```

> :warning: **Dla kontenera docker, przed instalacją pakietu webmin**:   
Należy wykonać poniższe polecenia, gdyż pakiet apt-show-versions nie działa gdy flaga **Acquire::GzipIndexes** jest ustawiona na **true** w managerze pakietów.
```bash
rm /etc/apt/apt.conf.d/docker-gzip-indexes
apt purge apt-show-versions
rm /var/lib/apt/lists/*lz4
apt -o Acquire::GzipIndexes=false update
apt install apt-show-versions
```

Pobrałem pakiet **webmin** w wersji **1.941**, zainstalowałem ją oraz usunąłem poleceniem:
```bash
 wget https://prdownloads.sourceforge.net/webadmin/webmin_1.941_all.deb -O webmin.deb && dpkg -i "$_" && rm "$_"
```

#### Linux Firewall
> :warning: **Dla kontenera docker, przed konfiguracją Linux Firewall**:  
Niestety na obrazie który pobrałem potrzebne było doinstalowanie dodatkowych pakietów do poprawnego działania Linux Firewall. Doinstalowałem je poleceniem:
```bash
apt install iptables ifupdown
```

Następnie skonfigurowałem firewall tak jak w tutorialu. Usługa **apache2** na porcie **80** przestała odpowiadać.  
Aby nie usuwać reguły, a umożliwić dostęp do usługi **apache2** wystawionej na porcie **80**, należy zmienić akcję z **Drop** na **Accept**.

### Zadanie 4

Zainstalowałem pakiety **slapd** oraz **ldap-utils** poleceniem:
```bash
apt install slapd ldap-utils
```

Przekonfigurowałem pakiet **slapd** poleceniem:
```bash
dpkg-reconfigure slapd
```
+ Zamiast domeny **lab.pl** podałem wczesniej utworzoną **lab.lokalne**.

Otworzyłem do edycji plik **/etc/ldap/ldap.conf** poleceniem:
```bash
nano -w /etc/ldap/ldap.conf
```

W LXplorerze utworzyłem użytkownika zgodnie z opisem.

W katalogu domowym stworzylem katalog **ldap** poleceniem:
```bash
mkdir ~/ldap
```

Zapisałem zawartość obiektu **user1** do pliku **user1.ldif** poleceniem:
```bash
ldapsearch -x -LLL uid=user1 > user1.ldif
```

Wyświetliłem plik **user1.ldif** poleceniem:
```bash
cat user1.ldif
```
Wynik:
```text
dn: cn=user1,cn=admin,dc=lab,dc=lokalne
cn: user1
gidNumber: 100
homeDirectory: /home/user1
objectClass: posixAccount
objectClass: person
objectClass: simpleSecurityObject
objectClass: top
sn: Nowak
uid: user1
uidNumber: 100001

```

### Zadanie 5
Udało mi się zaimplementować nastepujący skrypt w bash'u:
```bash
#!/bin/bash

#default values
group_id=100
admin_username="admin"
base_dn="dc=lab,dc=lokalne"
default_objects=("posixAccount" "person" "simpleSecurityObject" "top")
tmp_template_path=$(mktemp)

#User interface
read -p 'Enter first name: ' first_name
read -p 'Enter second name: ' last_name
read -sp 'Enter password: ' password

declare -l username="${first_name:0:1}${last_name}"

home_directory="/home/${username}"

next_uid=$[($(ldapsearch -x -LLL uidnumber | grep "uidNumber" | cut -d" " -f2 | sort -n -r | head -1))+1]

template="dn: cn=${username},cn=${admin_username},${base_dn}
cn: ${username}
gidNumber: ${group_id}
homeDirectory: ${home_directory}
sn: ${last_name}
uid: ${username}
uidNumber: ${next_uid}
userpassword: ${password}"

echo "${template}" >> ${tmp_template_path}

for ix in ${!default_objects[*]}
do
    echo "objectClass: ${default_objects[$ix]}" >> ${tmp_template_path}
done

cat ${tmp_template_path}

read -p "Continue? (y/n): " choice
case "$choice" in
  y|Y ) echo "Creating user: ${username}";;
  n|N ) echo "Aborted by user.";exit;;
  * ) echo "invalid";;
esac

ldapmodify -a -x -W -D "cn=${admin_username},${base_dn}" -f ${tmp_template_path}

if [[ "$?" = "0" ]]
then
    echo "Creating home directory: ${home_directory}"
    mkdir ${home_directory}
    chmod 700 ${home_directory}
    chown ${username}:${group_id} ${home_directory}
fi

rm -f ${tmp_template_path}
```

Aby ułatwić sobie development utworzyłem sobie także skrypt który usunie mi użytkownika o danym username:
```bash
#!/bin/bash

#default values
admin_username="admin"
base_dn="dc=lab,dc=lokalne"

#User interface
read -p 'Enter username: ' username

ldapdelete -x -v -D "cn=${admin_username},${base_dn}" -W "cn=${username},cn=${admin_username},${base_dn}"

if [[ "$?" = "0" ]]
then
    read -p "Delete user home directory? (y/n): " choice
    case "$choice" in
        y|Y ) rm -rf "/home/${username}";echo "Deleted home directory: /home/${username}";;
        n|N ) ;;
        * ) echo "invalid";;
esac
    echo "Deleted user: ${username}"
fi
```

### Zadanie 6
Zainstalowałem pakiet **libnss-ldap** poleceniem:
```bash
apt get libnss-ldap
```

Skonfigurowałem plik **ldap.conf** zgodnie z treścią zadania:
```conf
###DEBCONF###
##
## Configuration of this file will be managed by debconf as long as the
## first line of the file says '###DEBCONF###'
##
## You should use dpkg-reconfigure to configure this file via debconf
##

#
# @(#)$Id: ldap.conf,v 1.38 2006/05/15 08:13:31 lukeh Exp $
#
# This is the configuration file for the LDAP nameservice
# switch library and the LDAP PAM module.
#
# PADL Software
# http://www.padl.com
#

# Your LDAP server. Must be resolvable without using LDAP.
# Multiple hosts may be specified, each separated by a 
# space. How long nss_ldap takes to failover depends on
# whether your LDAP client library supports configurable
# network or connect timeouts (see bind_timelimit).
#host 127.0.0.1

# The distinguished name of the search base.
base dc=lab,dc=lokalne

# Another way to specify your LDAP server is to provide an
uri ldap://localhost
# Unix Domain Sockets to connect to a local LDAP Server.
#uri ldap://127.0.0.1/
#uri ldaps://127.0.0.1/   
#uri ldapi://%2fvar%2frun%2fldapi_sock/
# Note: %2f encodes the '/' used as directory separator

# The LDAP version to use (defaults to 3
# if supported by client library)
ldap_version 3

# The distinguished name to bind to the server with.
# Optional: default is to bind anonymously.
binddn cn=admin,dc=lab,dc=lokalne

# The credentials to bind with. 
# Optional: default is no credential.
bindpw 12345678

# The distinguished name to bind to the server with
# if the effective user ID is root. Password is
# stored in /etc/ldap.secret (mode 600)
rootbinddn cn=admin,dc=lab,dc=lokalne

# The port.
# Optional: default is 389.
#port 389

# The search scope.
scope sub
#scope one
#scope base

# Search timelimit
#timelimit 30

# Bind/connect timelimit
#bind_timelimit 30

# Reconnect policy: hard (default) will retry connecting to
# the software with exponential backoff, soft will fail
# immediately.
#bind_policy hard

# Idle timelimit; client will close connections
# (nss_ldap only) if the server has not been contacted
# for the number of seconds specified below.
#idle_timelimit 3600

# Filter to AND with uid=%s
#pam_filter objectclass=account

# The user ID attribute (defaults to uid)
#pam_login_attribute uid

# Search the root DSE for the password policy (works
# with Netscape Directory Server)
#pam_lookup_policy yes

# Check the 'host' attribute for access control
# Default is no; if set to yes, and user has no
# value for the host attribute, and pam_ldap is
# configured for account management (authorization)
# then the user will not be allowed to login.
#pam_check_host_attr yes

# Check the 'authorizedService' attribute for access
# control
# Default is no; if set to yes, and the user has no
# value for the authorizedService attribute, and
# pam_ldap is configured for account management
# (authorization) then the user will not be allowed
# to login.
#pam_check_service_attr yes

# Group to enforce membership of
#pam_groupdn cn=PAM,ou=Groups,dc=padl,dc=com

# Group member attribute
#pam_member_attribute uniquemember

# Specify a minium or maximum UID number allowed
#pam_min_uid 0
#pam_max_uid 0

# Template login attribute, default template user
# (can be overriden by value of former attribute
# in user's entry)
#pam_login_attribute userPrincipalName
#pam_template_login_attribute uid
#pam_template_login nobody

# HEADS UP: the pam_crypt, pam_nds_passwd,
# and pam_ad_passwd options are no
# longer supported.
#
# Do not hash the password at all; presume
# the directory server will do it, if
# necessary. This is the default.
pam_password clear

# Hash password locally; required for University of
# Michigan LDAP server, and works with Netscape
# Directory Server if you're using the UNIX-Crypt
# hash mechanism and not using the NT Synchronization
# service. 
#pam_password crypt

# Remove old password first, then update in
# cleartext. Necessary for use with Novell
# Directory Services (NDS)
#pam_password clear_remove_old
#pam_password nds

# RACF is an alias for the above. For use with
# IBM RACF
#pam_password racf

# Update Active Directory password, by
# creating Unicode password and updating
# unicodePwd attribute.
#pam_password ad

# Use the OpenLDAP password change
# extended operation to update the password.
#pam_password exop

# Redirect users to a URL or somesuch on password
# changes.
#pam_password_prohibit_message Please visit http://internal to change your password.

# RFC2307bis naming contexts
# Syntax:
# nss_base_XXX          base?scope?filter
# where scope is {base,one,sub}
# and filter is a filter to be &'d with the
# default filter.
# You can omit the suffix eg:
# nss_base_passwd       ou=People,
# to append the default base DN but this
# may incur a small performance impact.
#nss_base_passwd        ou=People,dc=padl,dc=com?one
#nss_base_shadow        ou=People,dc=padl,dc=com?one
#nss_base_group         ou=Group,dc=padl,dc=com?one
#nss_base_hosts         ou=Hosts,dc=padl,dc=com?one
#nss_base_services      ou=Services,dc=padl,dc=com?one
#nss_base_networks      ou=Networks,dc=padl,dc=com?one
#nss_base_protocols     ou=Protocols,dc=padl,dc=com?one
#nss_base_rpc           ou=Rpc,dc=padl,dc=com?one
#nss_base_ethers        ou=Ethers,dc=padl,dc=com?one
#nss_base_netmasks      ou=Networks,dc=padl,dc=com?ne
#nss_base_bootparams    ou=Ethers,dc=padl,dc=com?one
#nss_base_aliases       ou=Aliases,dc=padl,dc=com?one
#nss_base_netgroup      ou=Netgroup,dc=padl,dc=com?one

# attribute/objectclass mapping
# Syntax:
#nss_map_attribute      rfc2307attribute        mapped_attribute
#nss_map_objectclass    rfc2307objectclass      mapped_objectclass

# configure --enable-nds is no longer supported.
# NDS mappings
#nss_map_attribute uniqueMember member

# Services for UNIX 3.5 mappings
#nss_map_objectclass posixAccount User
#nss_map_objectclass shadowAccount User
#nss_map_attribute uid msSFU30Name
#nss_map_attribute uniqueMember msSFU30PosixMember
#nss_map_attribute userPassword msSFU30Password
#nss_map_attribute homeDirectory msSFU30HomeDirectory
#nss_map_attribute homeDirectory msSFUHomeDirectory
#nss_map_objectclass posixGroup Group
#pam_login_attribute msSFU30Name
#pam_filter objectclass=User
#pam_password ad

# configure --enable-mssfu-schema is no longer supported.
# Services for UNIX 2.0 mappings
#nss_map_objectclass posixAccount User
#nss_map_objectclass shadowAccount user
#nss_map_attribute uid msSFUName
#nss_map_attribute uniqueMember posixMember
#nss_map_attribute userPassword msSFUPassword
#nss_map_attribute homeDirectory msSFUHomeDirectory
#nss_map_attribute shadowLastChange pwdLastSet
#nss_map_objectclass posixGroup Group
#nss_map_attribute cn msSFUName
#pam_login_attribute msSFUName
#pam_filter objectclass=User
#pam_password ad

# RFC 2307 (AD) mappings
#nss_map_objectclass posixAccount user
#nss_map_objectclass shadowAccount user
#nss_map_attribute uid sAMAccountName
#nss_map_attribute homeDirectory unixHomeDirectory
#nss_map_attribute shadowLastChange pwdLastSet
#nss_map_objectclass posixGroup group
#nss_map_attribute uniqueMember member
#pam_login_attribute sAMAccountName
#pam_filter objectclass=User
#pam_password ad

# configure --enable-authpassword is no longer supported
# AuthPassword mappings
#nss_map_attribute userPassword authPassword

# AIX SecureWay mappings
#nss_map_objectclass posixAccount aixAccount
#nss_base_passwd ou=aixaccount,?one
#nss_map_attribute uid userName
#nss_map_attribute gidNumber gid
#nss_map_attribute uidNumber uid
#nss_map_attribute userPassword passwordChar
#nss_map_objectclass posixGroup aixAccessGroup
#nss_base_group ou=aixgroup,?one
#nss_map_attribute cn groupName
#nss_map_attribute uniqueMember member
#pam_login_attribute userName
#pam_filter objectclass=aixAccount
#pam_password clear

# Netscape SDK LDAPS
#ssl on

# Netscape SDK SSL options
#sslpath /etc/ssl/certs

# OpenLDAP SSL mechanism
# start_tls mechanism uses the normal LDAP port, LDAPS typically 636
#ssl start_tls
#ssl on

# OpenLDAP SSL options
# Require and verify server certificate (yes/no)
# Default is to use libldap's default behavior, which can be configured in
# /etc/openldap/ldap.conf using the TLS_REQCERT setting.  The default for
# OpenLDAP 2.0 and earlier is "no", for 2.1 and later is "yes".
#tls_checkpeer yes

# CA certificates for server certificate verification
# At least one of these are required if tls_checkpeer is "yes"
#tls_cacertfile /etc/ssl/ca.cert
#tls_cacertdir /etc/ssl/certs

# Seed the PRNG if /dev/urandom is not provided
#tls_randfile /var/run/egd-pool

# SSL cipher suite
# See man ciphers for syntax
#tls_ciphers TLSv1

# Client certificate and key
# Use these, if your server requires client authentication.
#tls_cert
#tls_key

# Disable SASL security layers. This is needed for AD.
#sasl_secprops maxssf=0

# Override the default Kerberos ticket cache location.
#krb5_ccname FILE:/etc/.ldapcache

# SASL mechanism for PAM authentication - use is experimental
# at present and does not support password policy control
#pam_sasl_mech DIGEST-MD5
```

Wynikiem wykonania następującego polecenia:
```bash
id imaculewicz
```
było:
```text
uid=100002(imaculewicz) gid=100(users) groups=100(users)
```

### Zadanie 7

Sprawdzilem czy istnieje pakiet **libpam-ldap** poleceniem:
```bash
apt install libpam-ldap
```
pakiet był już zainstalowany.

Sprawdziłem czy w pliku **common-account** znajduje się wpis **account** z **pam_ldap.so** poleceniem:
```bash
cat common-account | grep success=1
```
rezultat:
```text
account [success=1 default=ignore]      pam_ldap.so 
```

Sprawdziłem czy w pliku **common-auth** znajduje się wpis **auth** z **pam_ldap.so use_first_pass** poleceniem:
```bash
cat common-auth | grep success=1
```
rezultat:
```text
auth    [success=1 default=ignore]      pam_ldap.so use_first_pass
```

Sprawdziłem czy w pliku **common-password** znajduje się wpis **password** z **pam_ldap.so use_authtok try_first_pass** poleceniem:
```bash
cat common-auth | grep success=1
```
rezultat:
```text
password        [success=1 user_unknown=ignore default=die]     pam_ldap.so use_authtok try_first_pass
```

Sprawdziłem czy w pliku **common-session** znajduje się wpis **session** z **pam_ldap.so** poleceniem:
```bash
cat common-session | grep pam_ldap.so
```
rezultat:
```text
session optional                        pam_ldap.so 
```

Sprawdziłem czy w pliku **common-session-noninteractive** znajduje się wpis **session** z **pam_ldap.so** poleceniem:
```bash
cat common-session-noninteractive | grep pam_ldap.so
```
rezultat:
```text
session optional                        pam_ldap.so 
```

### Zadanie 8
Certyfikaty SSL podpisywane podpisem za pomocą funkcji skrótu SHA1 zostały uznane za niebezpieczne. 
Także niebezpieczne jest wydawanie certyfikatu SSL na okres tak długi jak 10 lat.
Żadna z norm SSL nie zezwala już na to. 
Normy mówią o tym że certyfikat SSL może zostać wydany maksymalnie na 2 lata.
Stworzyłem więc certyfikat podpisany funkcją skrótu SHA-256 na okres dwóch lat.


Przeszedłem do katalogu domowego poleceniem:
```bash
cd ~/
```

Utworzyłem katalog **openssl** i przeszedłem do niego poleceniem:
```bash
mkdir openssl && cd "$_"
```

Utworzyłem katalog **ca** i przeszedlem do niego poleceniem:
```bash
mkdir ca && cd "$_"
```

Wygenerowałem klucz prywatny dla CA poleceniem:
```bash
 openssl genrsa -out ca.key 4096
```

Scertyfikowałem klucz publiczny certyfikatem poleceniem:
```bash
openssl req -new -x509 -days 3650 -key ca.key -out ca.crt
```

Przeszedłem do katalogu wyżej poleceniem:
```bash
cd ..
```

Wygenerowałem klucz prywatny dla certyfikatu docelowego poleceniem:
```bash
openssl genrsa -out 172_17_0_2.pem 1024
```
> :warning: **Klucze o długości 1024 bit**: Nie zaleca się generowania tak krótkich kluczy.  
Zaleca się generowanie kluczy o długości co najmniej 2048 bit.  

Wygenerowałem żądanie podpisania certyfikatu docelowego dla wcześniej wygenerowanego klucza prywatnego: 
```bash
openssl req -new -key 172_17_0_2.pem -out 172_17_0_2.csr
```

Kluczem prywatnym CA podpisałem certyfikat dla klucza prywatnego certyfikatu docelowego, oraz zarejestrowałem go w CA:
```bash
openssl x509 -req -in 172_17_0_2.csr -out 172_17_0_2-cert.pem -sha256 -CA ./ca/ca.crt -CAkey ./ca/ca.key -CAcreateserial -days 730
```

Skleiłem certyfikat docelowy razem z powiązanym z nim kluczem prywatnym:
```bash
cat 172_17_0_2-cert.pem >> 172_17_0_2.pem 
```
plik wygląda następująco:
```text
-----BEGIN RSA PRIVATE KEY-----
MIICXgIBAAKBgQDcwU6esaG/2cnP1DmCK5go/qdDnzZrn9TMfGX00eGs1u01eqkl
buFARC7IW2dWYDffohjXVdwJX40DMQQayzZK32rOSBgSY7mvBFJoJSXabZ+o8EVD
cs4t/Yb8HdPsqsvjrIyWwJxAe46DgreOVhcsni9wo+WYfTsrIEhEaumIHQIDAQAB
AoGBAMOUZCJHACYoPxtpS/Yex3VygCByG1XoyjDM+cuYc1edJ5Xc8aTfgqNchRIv
oPMu1fHE/Dszr3mCdT1hnOBcL+EqpH/P3mAYgBt8pQOPwz+Fyfs02tcpTloFTCSy
cDGrouzC68P3ALioJQ7urARRKHsJiDwXDoHkUnIR1csGkL3FAkEA8Abnz1n7J3Bw
RboyiMWJY/Oaq3BLGJRx7JcmYbYhpclDAks78jowz4q6TdYAizcLGHws+BbINlWf
Zo2GdmWmDwJBAOtyFiGg7ORX3YtxiYGgrY0/bl5Fi49DeiKtjwwPmHd4RFLUy/W6
+qaAGPUzuXsEIAyyxs+uaXtU2pILbEkbexMCQQDQG8yk9UlTGqPdcOEbwNmsdawm
9IfH2f0kurCmi3mE+olE1eI11Mo5R/jcP63dBm5yXkfHGkmhAn/gNNjekqd1AkAU
t+JYrTeLVGwLZD9MZ949yrIkm4wPXEm4GXu3/PtWr/+bJlyYUA7UbftEQHv25kMH
uwehqOBhWPMu3NB3knJ/AkEAnywkB1fjB+KiTmKyy+m+93FqCLWGvo9HYiSgmBMG
ZUdEqCfsJ4+U4O8XsLFYfVejgXwXGUP5no2cPyQJPpopwA==
-----END RSA PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
MIIEJTCCAg0CFHbJehE4axWsnNgj/N6Xh/vJO65BMA0GCSqGSIb3DQEBCwUAMIGQ
MQswCQYDVQQGEwJQTDEbMBkGA1UECAwSWmFjaG9kbmlvcG9tb3Jza2llMREwDwYD
VQQHDAhTemN6ZWNpbjEsMCoGA1UECgwjWmFjaG9kbmlvcG9tb3Jza2EgU3prb8OF
woJhIEJpem5lc3UxDjAMBgNVBAsMBUFpc21OMRMwEQYDVQQDDAoxNzIuMTcuMC4y
MB4XDTIwMDUyMTAxNDkwMloXDTIyMDUyMTAxNDkwMlowgZAxCzAJBgNVBAYTAlBM
MRswGQYDVQQIDBJaYWNob2RuaW9wb21vcnNraWUxETAPBgNVBAcMCFN6Y3plY2lu
MSwwKgYDVQQKDCNaYWNob2RuaW9wb21vcnNrYSBTemtvw4XCgmEgQml6bmVzdTEO
MAwGA1UECwwFQWlzbU4xEzARBgNVBAMMCjE3Mi4xNy4wLjIwgZ8wDQYJKoZIhvcN
AQEBBQADgY0AMIGJAoGBANzBTp6xob/Zyc/UOYIrmCj+p0OfNmuf1Mx8ZfTR4azW
7TV6qSVu4UBELshbZ1ZgN9+iGNdV3AlfjQMxBBrLNkrfas5IGBJjua8EUmglJdpt
n6jwRUNyzi39hvwd0+yqy+OsjJbAnEB7joOCt45WFyyeL3Cj5Zh9OysgSERq6Ygd
AgMBAAEwDQYJKoZIhvcNAQELBQADggIBAJH82P9qPeGXTkKBwPUdplkvQuN6JiYs
tD3c67p+hawlW32OpmeoiUnEjergcLdysjcg/UV3ZIhjCWzgw+EvTuRatym4Luwu
G/Z1Rfqhv6R4BZIhnengdqA7vgg1busaPxYPEhUU+KXHM6HBLZz/hW6h7fS4oVbT
uDw2k54ckjJfwtzXqcnxjV00S6mgYgMjAehiRvDgyVODkWjA1kd392aQs4p657G4
ddXr6+yRsxVo0LnYvPE/M/1rY++zOF1dotz1ZCiW2SWmoAfGjhnFODBsHkuk9ytm
jq8tLpt2qWy261/v4GU7HsagwO8EGu3hU0YXmcbdjoHuI5t1nu6D4UZwYqJuRlE4
BQGfr5Vo8KhrVYuJLB0Ngf+BsQoY92O19Dcl8mbHnzF9VCnDaIT4Wmbz5kMgdj4p
2t2RMtvMEZHXVfY1C0FDOoaofTyqXGX+d5lZcr41S+ZOa4dLrMTtQY2WObEYF2Uh
tRhl4vvC38xGLbrhNGBygXOYHyb6+RgiSbzX9Plyjtj85r9D1EQgwco/LkEcGT4l
z3l5LH4V3D6XQ8R3wJ6niryhdOvnEBJIjqdfNqUDMzTq4IBq6sVSM4pVef0KdHbH
jMqJRQ7Z1lhFDhCxmhBFnSgVA8nc4zwPwRNRdMWBSegnD/9pzN/n7buSUik80Mnk
tTiubS44oS88
-----END CERTIFICATE-----
```

Utworzyłem katalog **/etc/apache2/ssl** poleceniem;
```bash
mkdir /etc/apache2/ssl
```

Skopiowałem parę(certyfikat i powiązany z nim klucz prywatny) do katalogu **/etc/apache2/ssl** poleceniem:
```bash
cp 172_17_0_2.pem /etc/apache2/ssl
```

W pliku **/etc/apache2/sites-available/default-ssl** podmieniłem wartości zgodnie z treścią zadania poleceniami:
```bash
sed -i 's#/etc/ssl/certs/ssl-cert-snakeoil.pem#/etc/apache2/ssl/172_17_0_2.pem#g' /etc/apache2/sites-available/default-ssl.conf
sed -i 's/SSLCertificateKeyFile/#SSLCertificateKeyFile/g' /etc/apache2/sites-available/default-ssl.conf
```

Po wykonaniu poleceń wyświetliłem plik aby sprawdzić czy się udało poleceniem:
```bash
head -n 50 /etc/apache2/sites-available/default-ssl.conf
```
Wynik:
```text
<IfModule mod_ssl.c>
        <VirtualHost _default_:443>
                ServerAdmin webmaster@localhost

                DocumentRoot /var/www/html

                # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
                # error, crit, alert, emerg.
                # It is also possible to configure the loglevel for particular
                # modules, e.g.
                #LogLevel info ssl:warn

                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined

                # For most configuration files from conf-available/, which are
                # enabled or disabled at a global level, it is possible to
                # include a line for only one particular virtual host. For example the
                # following line enables the CGI configuration for this host only
                # after it has been globally disabled with "a2disconf".
                #Include conf-available/serve-cgi-bin.conf

                #   SSL Engine Switch:
                #   Enable/Disable SSL for this virtual host.
                SSLEngine on

                #   A self-signed (snakeoil) certificate can be created by installing
                #   the ssl-cert package. See
                #   /usr/share/doc/apache2/README.Debian.gz for more info.
                #   If both key and certificate are stored in the same file, only the
                #   SSLCertificateFile directive is needed.
                SSLCertificateFile      /etc/apache2/ssl/172_17_0_2.pem
                #SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

                #   Server Certificate Chain:
                #   Point SSLCertificateChainFile at a file containing the
                #   concatenation of PEM encoded CA certificates which form the
                #   certificate chain for the server certificate. Alternatively
                #   the referenced file can be the same as SSLCertificateFile
                #   when the CA certificates are directly appended to the server
                #   certificate for convinience.
                #SSLCertificateChainFile /etc/apache2/ssl.crt/server-ca.crt

                #   Certificate Authority (CA):
                #   Set the CA certificate verification path where to find CA
                #   certificates for client authentication or alternatively one
                #   huge file containing all of them (file must be PEM encoded)
                #   Note: Inside SSLCACertificatePath you need hash symlinks
                #                to point to the certificate files. Use the provided
                #                Makefile to update the hash symlinks after changes.
```

Włączyłem moduł SSL poleceniem:
```bash
a2enmod ssl
```

I aktywowałem strukturę dodatkową poleceniem:
```bash
a2ensite default-ssl
```

Zrestartowałem usługę **apache2** poleceniem:
```bash
/etc/init.d/apache2 restart
```

Po wejściu na adres **https://172.17.0.2/joomla** i kliknięciu w informację o certyfikacie pojawił się podpięty certyfikat.

### Zadanie 9

Zainstalowałem pakiet **samba** poleceniem:
```bash
apt install samba
```
Utworzyłem katalog **/shared** i nadałem mu uprawnienia 777 poleceniami:
```bash
mkdir /shared
chmod 777 /shared
```

Do pliku **/etc/samba/smb.conf** dodałem udział:
```smb
[zpsb-shared]
  path = /shared
  read only = no
  guest ok = yes
  veto files = /*.ini/*.inf/
  delete veto files = yes
```

Zrestartowałem usługe **smbd** poleceniem:
```bash
/etc/init.d/smbd restart
```

Po wykonaniu tych kroków, udało mi się połączyć z katalogiem sieciowym,  
stworzyć folder, stworzyć plik tekstowy. Nie udało się stworzyć pliku *.ini.